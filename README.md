### ISTRUZIONI ###

## Risolti i problemi relativi all'incompatibilita di nodejs, express e ES6 che non consentono l'utilizzo del meccanismo di Import. ##
# la soluzione e' utilizzare il type:module nel file package.json ed utilizzare il flag --experimental-modules come di seguito: 
1. nodejs --experimental-modules ./index.js

## Per monggose e' stato necessario utilizzare requirejs e quindi e' stato creato ( vedi documentazione nodejs ) il meccanismo per l'utilizzo di requirejs. ##

## Creati nuovi endpoit: ##

1. /search_user?id=mondodbId  GET           invocabile con: curl -X GET 'http://localhost:8080/search_user?id=5d931e75caa52c383ffc4615' 
2. /change_user  POST  body:doc_json        invocabile con: curl -X POST http://localhost:8080/change_user -H 'content-type: application/json' -d '{"_id":"5d931e75caa52c383ffc4615","email":"text2@example.com","firstName":"Pipp","lastName":"Paper","__v":0}' 
3. /remove_user/mongoId DELETE              invocabile con: curl -X DELETE http://localhost:8080/remove_user/5d931e75caa52c383ffc4615 
4. /remove_user  DELETE body:doc_json       invocabile con: curl -X DELETE http://localhost:8080/remove_user -H 'content-type: application/json' -d '{"_id":"5d931e75caa52c383ffc4615","email":"text1@example.com","firstName":"Pipp","lastName":"Paper","__v":0}' #