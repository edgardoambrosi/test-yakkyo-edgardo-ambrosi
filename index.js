/** AUTORE: Edgardo Ambrosi 
 * con la combinazione ( mongoose, nodejs, express ) il meccanismo di import di ES6 non funziona quindi si ricorre a require
 * require non disponibile in ES modules vedi https://nodejs.org/api/esm.html#esm_no_code_require_code_code_exports_code_code_module_exports_code_code_filename_code_code_dirname_code
 * require si rende disponibile attraverso https://nodejs.org/api/modules.html#modules_module_createrequire_filename
 
/*MODIFICHE APPORTATE PER USARE REQUIRE CON EXPRESS E ES6*/
import {createRequire} from 'module'
const require=createRequire(import.meta.url)

//import { connect } from 'mongoose'
var connect = require('mongoose').connect
/*MODIFICHE APPORTATE PER USARE REQUIRE CON EXPRESS E ES6*/

import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel.js'
import cors from 'cors'

connect('mongodb://test:yakkyo@ds135069.mlab.com:35069/test-yakkyo', { useNewUrlParser: true })

const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

app.get('/users', (req, res) => {
  UserModel.find((err, results) => {
    res.send(results)
  })
})



/**AUTORE: Edgardo Ambrosi
* Nuovi endpoint per la SEARCH di un utente.
* Utilizzo: .../search_user?id="mongodb id"
**/
app.route('/search_user')
	.get(function(req,res){
	  /**
	  * RICHIESTA PER TEST:
	  * http://localhost:8080/search_user?id=5d83331f2fbce36f44f45e53
	  **/	
	  UserModel.findById(req.query.id, (err, results) => {
		res.send(results)
	  })	
	}) 
/**AUTORE: Edgardo Ambrosi
* Nuovi endpoint per UPDATE di un utente.
* Utilizzo: .../change_user
* nel Body dalla request un json tipo: {"_id":"5d931e75caa52c383ffc4615","__v":0,"email":"text2@example.com","firstName":"Pipp","lastName":"Paper"}
* Nella chiamata post usare i seguenti parametri,
* url: "http://localhost:8080/change_user",
* header: "content-type": "application/json"
**/
app.route('/change_user')
	.post(function(req,res){
	  UserModel.findByIdAndUpdate(req.body._id, req.body,{new:true,upsert:true},(err,results)=>{
	  	res.send(results)
	  })
	}) 

/**AUTORE: Edgardo Ambrosi
* Nuovi endpoint per la DELETE di un utente.
* Utilizzo: .../remove_user/5d931e75caa52c383ffc4615
**/
app.route('/remove_user/:id')
	.delete(function(req,res){
		if (typeof(req.params.id) != 'undefined'){
		  UserModel.findByIdAndDelete(req.params.id,(result)=>{
		  	res.send(result)
		  })
		}else{
		  	res.send("MongoDB ID non specificato")
		}
	})
/**AUTORE: Edgardo Ambrosi
* Nuovi endpoint per la DELETE di un utente.
* Utilizzo: .../remove_user
* nel Body dalla request un json tipo: {"_id":"5d931e75caa52c383ffc4615","__v":0,"email":"text2@example.com","firstName":"Pipp","lastName":"Paper"}
* Nella chiamata post usare i seguenti parametri,
* url: "http://localhost:8080/remove_user",
* header: "content-type": "application/json"
**/
app.route('/remove_user')	
	.delete(function(req,res){
	  UserModel.findByIdAndDelete(req.body._id,(result)=>{
	  	res.send(result)
	  })	
	}) 



app.post('/users', (req, res) => {
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

app.listen(8080, () => console.log('Example app listening on port 8080!'))