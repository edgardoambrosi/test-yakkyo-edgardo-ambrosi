/** AUTORE: Edgardo Ambrosi 
 * con la combinazione ( mongoose, nodejs, express ) il meccanismo di import di ES6 non funziona quindi si ricorre a require
 * require non disponibile in ES modules vedi https://nodejs.org/api/esm.html#esm_no_code_require_code_code_exports_code_code_module_exports_code_code_filename_code_code_dirname_code
 * require si rende disponibile attraverso https://nodejs.org/api/modules.html#modules_module_createrequire_filename
 
/*MODIFICHE APPORTATE PER USARE REQUIRE CON EXPRESS E ES6*/
import {createRequire} from 'module'
const require=createRequire(import.meta.url)

//import { Schema, Model } from 'mongoose'
var Schema = require('mongoose').Schema
var model = require('mongoose').model
/*MODIFICHE APPORTATE PER USARE REQUIRE CON EXPRESS E ES6*/

const UserSchema = new Schema({
  email: { type: String, lowercase: true, trim: true, required: true, unique: true },
  password: { type: String, required: true, select: false },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true }
})

export default model('User', UserSchema)